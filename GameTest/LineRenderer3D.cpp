#include "stdafx.h"
#include "App/app.h"
#include "LineRenderer3D.h"

mat4 LineRenderer3D::_view;
mat4 LineRenderer3D::_projection;
mat4 LineRenderer3D::_viewProjection;
mat4 LineRenderer3D::_model;
mat4 LineRenderer3D::_mvp;
mat3 LineRenderer3D::_colorKernel;
std::vector<mat4> LineRenderer3D::_transformStack;
float LineRenderer3D::_nearPlane;

void LineRenderer3D::SetView(const mat4& view) {
	_view = view;
	_viewProjection = _projection * _view;
	_mvp = _viewProjection * _model;
}

void LineRenderer3D::SetProjection(const mat4& projection) {
	_projection = projection;
	_viewProjection = _projection * _view;
	_mvp = _viewProjection * _model;

	// Extract near plane from projection
	float m22 = _projection[2][2];
	float m32 = _projection[3][2];
	_nearPlane = (2.0f * m32) / (2.0f * m22 - 2.0f);		
}

void LineRenderer3D::PushModelMatrix(const mat4& transform) {
	_transformStack.push_back(_model);
	_model = _model * transform;
	_mvp = _viewProjection * _model;
}

void LineRenderer3D::PopModelMatrix() {
	assert(_transformStack.empty() == false);

	_model = _transformStack.back();
	_transformStack.pop_back();
}

void LineRenderer3D::ResetModelMatrix() {
	_model = mat4();
	_mvp = _viewProjection * _model;
	_transformStack.clear();
}

inline void LineRenderer3D::SetColorKernel(const mat3& colorKernel) {
	_colorKernel = colorKernel;
}

void LineRenderer3D::ResetColorKernel(){
	SetColorKernel(mat3());
}

inline void LineRenderer3D::RenderLine(float sx, float sy, float sz, float ex, float ey, float ez, float r, float g, float b) {
	RenderLine({ sx, sy, sz }, { ex, ey, ez }, { r, g, b });
}

vec4 Lerp(const vec4& a, const vec4& b, float t) {
	float u = 1.0f - t;
	return u * a + t * b;
}

inline void LineRenderer3D::RenderLine(const vec3& start, const vec3& end, const vec3& color) {
	vec4 s = __Transform(_mvp, start);
	vec4 e = __Transform(_mvp, end);
	
	if ((s.W < _nearPlane) & (e.W < _nearPlane))
		return;

	if (s.W < _nearPlane) {
		float n = (s.W - _nearPlane) / (s.W - e.W);
		s = Lerp(s, e, n);
		s.W = _nearPlane;
	}
	if (e.W < _nearPlane) {
		float n = (e.W - _nearPlane) / (e.W - s.W);
		e = Lerp(e, s, n);
		e.W = _nearPlane;
	}
	s /= s.W;
	e /= e.W;
	vec3 c = _colorKernel * color;
	APP_NATIVE_TO_VIRTUAL_COORDS(s.X, s.Y);
	APP_NATIVE_TO_VIRTUAL_COORDS(e.X, e.Y);
	App::DrawLine(s.X, s.Y, e.X, e.Y, c.R, c.G, c.B);
}

void LineRenderer3D::RenderLineMesh(const LineMesh::Sptr& mesh) {
	for(const Line& line : mesh->_lines) {
		RenderLine(line.Start, line.End, line.Color);
	}
}

vec4 LineRenderer3D::__Transform(const mat4& mvp, const vec3& value) {
	vec4 result = vec4(value);
	result = mvp * result;
	return result;
}


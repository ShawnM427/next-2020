#pragma once
#include "IttyECS.hpp"
#include "LineRenderer3D.h"

class RenderableComponent : public IGameComponent
{
public:
	virtual ~RenderableComponent() = default;
	RenderableComponent* SetMesh(const LineMesh::Sptr& mesh);
	const LineMesh::Sptr& GetMesh() const { return _mesh; }
	void Render() override;

private:
	LineMesh::Sptr _mesh;
};
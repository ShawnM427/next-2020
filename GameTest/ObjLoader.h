#pragma once

#include "LineMesh.h"
#include <string>

class ObjLoader
{
public:
	static LineMesh::Sptr LoadMesh(const std::string& filename);
	static void SaveMeshBinary(const std::string& filename, const LineMesh::Sptr& mesh);
	static LineMesh::Sptr LoadMeshBinary(const std::string& filename);
};
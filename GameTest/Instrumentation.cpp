#include "stdafx.h"
#include "Instrumentation.h"

ProfileTimer::ProfileTimer(const std::string& name, bool start) {
	myName = name;
	isStopped = !start;
	myStartTime = time_now();
	myDuration = TimeResolution(0);
}

ProfileTimer::~ProfileTimer() {
	Stop();
}

void ProfileTimer::Restart() {
	isStopped = false;
	myStartTime = time_now();
}

void ProfileTimer::Stop() {
	if (!isStopped) {
		const Timepoint endTime = time_now();
		isStopped = true;
		myDuration = endTime - myStartTime;
	}
}
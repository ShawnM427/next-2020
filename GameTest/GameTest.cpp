//------------------------------------------------------------------------
// GameTest.cpp
//------------------------------------------------------------------------
#include "stdafx.h"
//------------------------------------------------------------------------
#include <windows.h>  
//------------------------------------------------------------------------
#include "App\app.h"
//------------------------------------------------------------------------
#include "LineRenderer3D.h"
#include "IttyECS.hpp"
#include "RenderableComponent.h"
#include "ObjLoader.h"
#include "IttyMath.hpp"
#include "Instrumentation.h"
#include "filesystem"
#include <iostream>

//------------------------------------------------------------------------
// Eample data....
//------------------------------------------------------------------------
CSimpleSprite *testSprite;
CSimpleSprite *testSprite2;
enum
{
	ANIM_FORWARDS,
	ANIM_BACKWARDS,
	ANIM_LEFT,
	ANIM_RIGHT,
};

mat4 view;

mat3 colorKernel;

LineMesh::Sptr CubeGeo;

GameScene TestScene;

class RotateComponent : public IGameComponent
{
public:
	float Speed;
	vec3  Axis;
	RotateComponent() : Speed(0.0f), Axis(vec3(0, 1, 0)) { }
	void Update(float deltaMS) override {
		mat4& transform = Object()->Transform;
		
		transform = mat4::Rotate(transform, Radians(Speed * deltaMS / 1000.0f), Axis);
	}
};

class RingPositionIndicator : public IGameComponent
{
public:
	RingPositionIndicator() : _channel(0), _activeColor(vec3::Green), _passiveColor(vec3::Yellow) {};
	virtual ~RingPositionIndicator() = default;

	void OnInit() override {
		RenderableComponent* renderer = nullptr;
		if (Object()->TryGetComponent(renderer)) {
			LineMesh::Sptr mesh = renderer->GetMesh();
			for (int ix = 0; ix < mesh->LineCount(); ix++)
				(*mesh)[ix].Color = _passiveColor;
			
			Line& activeLine = mesh->GetLine(_channel % mesh->LineCount());
			activeLine.Color = _activeColor;
		}
	}
	void Update(float deltaMs) override {
		if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_LEFT, true)) {
			SetChannel(_channel + 1);
		}
		if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_RIGHT, true)) {
			SetChannel(_channel - 1);
		}
	}
	void Render() override {
		RenderableComponent* renderer = nullptr;
		if (Object()->TryGetComponent(renderer)) {
			LineMesh::Sptr mesh = renderer->GetMesh();
			Line& activeLine = mesh->GetLine(_channel % mesh->LineCount());
			activeLine.Color = _activeColor;
		}
	}
	void SetChannel(int index) {
		RenderableComponent* renderer = nullptr;
		if (Object()->TryGetComponent(renderer)) {
			LineMesh::Sptr mesh = renderer->GetMesh();
			Line& passiveLine = mesh->GetLine(_channel % mesh->LineCount());
			passiveLine.Color = _passiveColor;
			_channel = static_cast<int>((index + mesh->LineCount()) % mesh->LineCount());
			Line& activeLine = mesh->GetLine(_channel % mesh->LineCount());
			activeLine.Color = _activeColor;
		}
	}
	size_t GetChannel() const { return _channel; }
	
private:
	int _channel;
	vec3 _activeColor;
	vec3 _passiveColor;
};

//------------------------------------------------------------------------

LineMesh::Sptr GenerateLevelGeo(int numSubdivisions, const vec3& color = vec3::White)
{
	assert(numSubdivisions > 2);

	const float radius = 2.0f;
	const float length = 200.0f;

	LineMesh::Sptr mesh = std::make_shared<LineMesh>();
	
	float arc = (PI * 2.0f) / (float)numSubdivisions;
	float angle = 0.0f;

	mesh->AddLine(vec3(radius, 0.0f, 0.0f), vec3(radius, 0.0f, length), color);
	for(int ix = 0; ix < numSubdivisions; ix++) {
		float x = cosf(angle) * radius;
		float y = sinf(angle) * radius;
		float x2 = cosf(angle + arc) * radius;
		float y2 = sinf(angle + arc) * radius;

		mesh->AddLine(vec3(x, y, 0.0f), vec3(x2, y2, 0.0f), color);
		mesh->AddLine(vec3(x2, y2, 0.0f), vec3(x2, y2, length), color);
		
		angle += arc;
	}
	return mesh;
}

LineMesh::Sptr GenerateRing(int numSubdivisions, const vec3& color = vec3::White)
{
	assert(numSubdivisions > 2);

	const float radius = 2.0f;
	const float length = 200.0f;

	LineMesh::Sptr mesh = std::make_shared<LineMesh>();

	float arc = (PI * 2.0f) / numSubdivisions;
	float angle = 0.0f;

	for (int ix = 0; ix < numSubdivisions; ix++) {
		float x = cosf(angle) * radius;
		float y = sinf(angle) * radius;
		float x2 = cosf(angle + arc) * radius;
		float y2 = sinf(angle + arc) * radius;

		mesh->AddLine(vec3(x, y, 0.0f), vec3(x2, y2, 0.0f), color);

		angle += arc;
	}
	return mesh;
}

//------------------------------------------------------------------------
// Called before first update. Do any initial setup here.
//------------------------------------------------------------------------
void Init()
{
	//------------------------------------------------------------------------
	// Example Sprite Code....
	

	testSprite = App::CreateSprite(".\\TestData\\Test.bmp", 8, 4);
	testSprite->SetPosition(400.0f, 400.0f);
	float speed = 1.0f / 15.0f;
	testSprite->CreateAnimation(ANIM_BACKWARDS, speed, { 0,1,2,3,4,5,6,7 });
	testSprite->CreateAnimation(ANIM_LEFT, speed, { 8,9,10,11,12,13,14,15 });
	testSprite->CreateAnimation(ANIM_RIGHT, speed, { 16,17,18,19,20,21,22,23 });
	testSprite->CreateAnimation(ANIM_FORWARDS, speed, { 24,25,26,27,28,29,30,31 });
	testSprite->SetScale(2.0f);

	testSprite2 = App::CreateSprite(".\\TestData\\Ships.bmp", 2, 12);
	testSprite2->SetPosition(400.0f, 400.0f);	
	testSprite2->SetFrame(2);
	testSprite2->SetScale(1.0f);
	//------------------------------------------------------------------------

	vec4 v1 = vec4(1, 2, 3, 4);
	v1.XY *= vec2(3, 4); // OK
	vec3 v2 = v1.XXW; // OK, not an assignment
	v2.YX *= 3; // OK, multiplying swizzle by scalar
	v1.ZW = v2.YX; // OK, swizzle-swizzle assignment
	//v2.XX = 0; // Error: deleted function
	//v1.XX += vec2(1, 1); // Error: deleted function
	//

	Color3 color = Color3::Blue;

	vec3 min = vec3(1.0f, 20.0f, 2.0f);
	vec3 max = vec3(5.0f, 100.0f, 4.0f);

	vec3 t = vec3(-10.0f, 50.0f, 100.0f);
	vec3 w = Wrap(t, min, max);
				
	mat4 projection = mat4::PerspectiveFovRH(Radians(60.0f), APP_VIRTUAL_WIDTH, APP_VIRTUAL_HEIGHT, 0.5f, 100.0f);
	view = mat4::LookAtRH(vec3(0, 0, -2.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	LineRenderer3D::SetView(view);
	LineRenderer3D::SetProjection(projection);

	// Mesh Creation
	{
		/*
		CubeGeo = std::make_shared<LineMesh>();
		CubeGeo->AddLine(vec3(-1.0f, -1.0f, -1.0f), vec3(-1.0f, +1.0f, -1.0f));
		CubeGeo->AddLine(vec3(+1.0f, -1.0f, -1.0f), vec3(+1.0f, +1.0f, -1.0f));
		CubeGeo->AddLine(vec3(-1.0f, -1.0f, -1.0f), vec3(+1.0f, -1.0f, -1.0f));
		CubeGeo->AddLine(vec3(-1.0f, +1.0f, -1.0f), vec3(+1.0f, +1.0f, -1.0f));

		CubeGeo->AddLine(vec3(-1.0f, -1.0f, +1.0f), vec3(-1.0f, +1.0f, +1.0f));
		CubeGeo->AddLine(vec3(+1.0f, -1.0f, +1.0f), vec3(+1.0f, +1.0f, +1.0f));
		CubeGeo->AddLine(vec3(-1.0f, -1.0f, +1.0f), vec3(+1.0f, -1.0f, +1.0f));
		CubeGeo->AddLine(vec3(-1.0f, +1.0f, +1.0f), vec3(+1.0f, +1.0f, +1.0f));

		CubeGeo->AddLine(vec3(-1.0f, -1.0f, -1.0f), vec3(-1.0f, -1.0f, +1.0f));
		CubeGeo->AddLine(vec3(+1.0f, -1.0f, -1.0f), vec3(+1.0f, -1.0f, +1.0f));
		CubeGeo->AddLine(vec3(-1.0f, +1.0f, -1.0f), vec3(-1.0f, +1.0f, +1.0f));
		CubeGeo->AddLine(vec3(+1.0f, +1.0f, -1.0f), vec3(+1.0f, +1.0f, +1.0f));
		*/
		
		// If we don't have a binary version of the model, load the OBJ version and create a binary 
		if (!std::filesystem::exists(".\\TestData\\monkey.bin")) {
			CubeGeo = ObjLoader::LoadMesh(".\\TestData\\monkey.obj");
			ObjLoader::SaveMeshBinary(".\\TestData\\monkey.bin", CubeGeo);
		}
		// Otherwise, we can just load the binary we created last run
		else {
			CubeGeo = ObjLoader::LoadMeshBinary(".\\TestData\\monkey.bin");
		}
		
		GameObject* rootObject = TestScene.CreateObject("Cube Thing");
		RotateComponent* rotate = rootObject->AddComponent<RotateComponent>();
		rotate->Axis = vec3(0, 0, 1);
		rotate->Speed = 45.0f;

		rootObject->AddChild()->AddComponent<RenderableComponent>()->SetMesh(CubeGeo)->Object()->Transform = mat4::Translate(vec3( 4.0f,  0.0f, 10.0f));
		rootObject->AddChild()->AddComponent<RenderableComponent>()->SetMesh(CubeGeo)->Object()->Transform = mat4::Translate(vec3(-4.0f,  0.0f, 10.0f));
		rootObject->AddChild()->AddComponent<RenderableComponent>()->SetMesh(CubeGeo)->Object()->Transform = mat4::Translate(vec3( 0.0f,  4.0f, 10.0f));
		rootObject->AddChild()->AddComponent<RenderableComponent>()->SetMesh(CubeGeo)->Object()->Transform = mat4::Translate(vec3( 0.0f, -4.0f, 10.0f));
	}

	{
		LineMesh::Sptr levelGeo = GenerateLevelGeo(10);
		
		GameObject* levelObject = TestScene.CreateObject("LevelGeo");
		RenderableComponent* levelRenderable = levelObject->FetchOrAssign<RenderableComponent>();
		levelRenderable->SetMesh(levelGeo);

		LineMesh::Sptr fixedRing = GenerateRing(10, vec3::Green);

		GameObject* staticRing = TestScene.CreateObject("IndicatorRing");
		staticRing->Transform = mat4::Translate(vec3(0.0f, 0.0f, 3.0f));
		RenderableComponent* ringRenderable = staticRing->FetchOrAssign<RenderableComponent>();
		ringRenderable->SetMesh(fixedRing);
		staticRing->AddComponent<RingPositionIndicator>();
	}

	TestScene.Init();
}

//------------------------------------------------------------------------
// Update your simulation here. deltaTime is the elapsed time since the last update in ms.
// This will be called at no greater frequency than the value of APP_MAX_FRAME_RATE
//------------------------------------------------------------------------
void Update(float deltaTime)
{
	float ds = deltaTime / 1000.0f;
	
	//------------------------------------------------------------------------
	// Example Sprite Code....
	testSprite->Update(deltaTime);
	testSprite2->Update(deltaTime);
	/*
	if (App::GetController().GetLeftThumbStickX() > 0.5f)
	{
		testSprite->SetAnimation(ANIM_RIGHT);
		float x, y;
		testSprite->GetPosition(x, y);
		x += 1.0f;
		testSprite->SetPosition(x, y);
	}
	if (App::GetController().GetLeftThumbStickX() < -0.5f)
	{
		testSprite->SetAnimation(ANIM_LEFT);
		float x, y;
		testSprite->GetPosition(x, y);
		x -= 1.0f;
		testSprite->SetPosition(x, y);
	}
	if (App::GetController().GetLeftThumbStickY() > 0.5f)
	{
		testSprite->SetAnimation(ANIM_FORWARDS);
		float x, y;
		testSprite->GetPosition(x, y);
		y += 1.0f;
		testSprite->SetPosition(x, y);
	}
	if (App::GetController().GetLeftThumbStickY() < -0.5f)
	{
		testSprite->SetAnimation(ANIM_BACKWARDS);
		float x, y;
		testSprite->GetPosition(x, y);
		y -= 1.0f;
		testSprite->SetPosition(x, y);
	}
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_UP, false))
	{
		testSprite->SetScale(testSprite->GetScale() + 0.1f);
	}
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_DOWN, false))
	{
		testSprite->SetScale(testSprite->GetScale() - 0.1f);
	}
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_LEFT, false))
	{
		testSprite->SetAngle(testSprite->GetAngle() + 0.1f);
	}
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_RIGHT, false))
	{
		testSprite->SetAngle(testSprite->GetAngle() - 0.1f);
	}
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_A, true))
	{
		testSprite->SetAnimation(-1);
	}
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_B, true))
	{
		testSprite->SetVertex(0, testSprite->GetVertex(0) + 5.0f);
	}
	*/
	//------------------------------------------------------------------------
	// Sample Sound.
	//------------------------------------------------------------------------
	if (App::GetController().CheckButton(XINPUT_GAMEPAD_B, true))
	{
		App::PlaySound(".\\TestData\\Test.wav");
	}

	view = mat4::Rotate(view, Radians(3.0f * ds), vec3(0, 0, 1));
	LineRenderer3D::SetView(view);
	colorKernel = mat3::Rotate(colorKernel, Radians(30.0f * ds));

	TestScene.Update(deltaTime);
}

//------------------------------------------------------------------------
// Add your display calls here (DrawLine,Print, DrawSprite.) 
// See App.h 
//------------------------------------------------------------------------
void Render()
{
	LineRenderer3D::ResetModelMatrix();
	LineRenderer3D::ResetColorKernel();

	TestScene.Render();
		
	//LineRenderer3D::PushModelMatrix(transform);
	//LineRenderer3D::SetColorKernel(colorKernel);
	//LineRenderer3D::RenderLineMesh(CubeGeo);
	
	//------------------------------------------------------------------------
	// Example Sprite Code....
	testSprite->Draw();
	testSprite2->Draw();
	//------------------------------------------------------------------------

	//------------------------------------------------------------------------
	// Example Text.
	//------------------------------------------------------------------------
	App::Print(100, 100, "Sample Text");

}
//------------------------------------------------------------------------
// Add your shutdown code here. Called when the APP_QUIT_KEY is pressed.
// Just before the app exits.
//------------------------------------------------------------------------
void Shutdown()
{	
	//------------------------------------------------------------------------
	// Example Sprite Code....
	delete testSprite;
	delete testSprite2;
	//------------------------------------------------------------------------
}
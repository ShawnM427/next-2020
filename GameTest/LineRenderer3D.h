#pragma once
#include "IttyMath.hpp"
#include "LineMesh.h"

class LineRenderer3D
{
public:
	static void SetView(const mat4& view);
	static void SetProjection(const mat4& projection);
	
	static void PushModelMatrix(const mat4& transform);
	static void PopModelMatrix();
	/*
	 * Resets the model matrix to the identity and clears the transformation stack
	 */
	static void ResetModelMatrix();

	static void SetColorKernel(const mat3& colorKernel);
	static void ResetColorKernel();
	
	static void RenderLine(float sx, float sy, float sz, float ex, float ey, float ez, float r = 1.0f, float g = 1.0f, float b = 1.0f);
	static void RenderLine(const vec3& start, const vec3& end, const vec3& color = vec3::White);

	static void RenderLineMesh(const LineMesh::Sptr& mesh);
		
private:
	static mat4 _view;
	static mat4 _projection;
	static mat4 _viewProjection;
	static mat4 _model;
	static mat4 _mvp;
	static mat3 _colorKernel;
	static float _nearPlane;

	static std::vector<mat4> _transformStack;

	static vec4 __Transform(const mat4& mvp, const vec3& value);
};




#pragma once

#include "IttyMath.hpp"
#include <vector>

struct Line {
	vec3 Start;
	vec3 End;
	vec3 Color;
};

/*
 * Represents a 3D mesh that is made up of lines
 */
class LineMesh
{
public:
	typedef std::shared_ptr<LineMesh> Sptr;
	typedef std::unique_ptr<LineMesh> Uptr;
	
	LineMesh();

	void AddLine(const Line& line);
	void AddLine(const vec3& start, const vec3& end, const vec3& color = vec3::White);
	void AddLine(float sx, float sy, float ex, float ey, float r = 1.0f, float g = 1.0f, float b = 1.0f);
	void AddLine(float sx, float sy, float sz, float ex, float ey, float ez, float r = 1.0f, float g = 1.0f, float b = 1.0f);

	Line& GetLine(size_t index) {
		assert(index >= 0 && index < _lines.size());
		return _lines[index];
	}
	const Line& GetLine(size_t index) const {
		assert(index >= 0 && index < _lines.size());
		return _lines[index];
	}
	
	Line& operator[](size_t index) { return GetLine(index); }
	Line operator[](size_t index) const { return GetLine(index); }
	
	LineMesh& operator +=(const Line& line);

	size_t LineCount() const { return _lines.size(); }
	
private:
	friend class LineRenderer3D;

	std::vector<Line> _lines;
};